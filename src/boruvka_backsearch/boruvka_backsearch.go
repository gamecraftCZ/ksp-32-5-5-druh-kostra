package main

// Inspired by page 3 of http://web.mit.edu/6.263/www/quiz1-f05-sol.pdf

import (
	"fmt"
	"io/ioutil"
	"math"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Line struct {
	sat1        *Satellite
	sat2        *Satellite
	cost        int
	index       int
	sortedIndex int
	used        bool
}

func (line Line) getNeighbour(currentSat *Satellite) *Satellite {
	if line.sat1 == currentSat {
		return line.sat2
	} else {
		return line.sat1
	}
}

type Satellite struct {
	lines []*Line
	index int

	usedLines []*Line

	visitedGetPath bool
}

func root(a int, trees []int) int {
	for trees[a] != -1 {
		trees[a] = root(trees[a], trees) // Flattening of root
		a = trees[a]
	}
	return a
}

func find(a, b int, trees []int) bool {
	return root(a, trees) == root(b, trees)
}

func connect(a, b int, trees []int) {
	trees[root(b, trees)] = a
}

func getMinSkell(lines []*Line, satellitesCount int) (int, []*Line) {

	trees := make([]int, satellitesCount)
	for i := 0; i < satellitesCount; i++ {
		trees[i] = -1
	}
	minSkell := make([]*Line, satellitesCount-1)

	connectionsIndex := 0
	cost := 0
	for i, line := range lines {
		if !find(line.sat1.index, line.sat2.index, trees) {
			line.sat1.usedLines = append(line.sat1.usedLines, line)
			line.sat2.usedLines = append(line.sat2.usedLines, line)
			connect(line.sat1.index, line.sat2.index, trees)
			line.sortedIndex = i
			line.used = true
			minSkell[connectionsIndex] = line
			cost += line.cost
			connectionsIndex++
		}
	}

	return cost, minSkell
}

func getCostliestLineDeepFirst(startPoint *Satellite, endPoint *Satellite, dontUseLine *Line, currentCostliestLine *Line) *Line {
	// As we are using deep search and there is no cycle in the graph, we can omit just the backpath satellite
	for _, line := range startPoint.usedLines {
		if line == dontUseLine {
			continue
		}
		neighbour := line.getNeighbour(startPoint)

		if line.cost > currentCostliestLine.cost {
			currentCostliestLine = line
		}

		if neighbour == endPoint {
			return currentCostliestLine
		}

		result := getCostliestLineDeepFirst(neighbour, endPoint, line, currentCostliestLine)
		if result != nil {
			return result
		}
	}

	return nil
}

func getPath(startPoint *Satellite, endPoint *Satellite) []*Line {
	visitedSatsPath := map[int][]*Line{startPoint.index: {}}
	toCheckQueue := []*Satellite{startPoint} // Add first satellite to queue

	for len(toCheckQueue) > 0 {
		sat := toCheckQueue[0]
		currentSatVisited := visitedSatsPath[sat.index]
		toCheckQueue = toCheckQueue[1:] // Remove satellite from queue - may not garbage collect, but ok in this case! https://stackoverflow.com/a/26863706/5647384

		for _, line := range sat.usedLines {
			neighbour := line.getNeighbour(sat)

			visited, _ := visitedSatsPath[neighbour.index]
			if visited != nil {
				continue
			}

			var newVisitedSats []*Line
			if currentSatVisited == nil {
				newVisitedSats = []*Line{line}
			} else {
				newVisitedSats = make([]*Line, len(currentSatVisited))
				copy(newVisitedSats, currentSatVisited)
				newVisitedSats = append(newVisitedSats, line)
			}
			visitedSatsPath[neighbour.index] = newVisitedSats

			if neighbour == endPoint {
				return visitedSatsPath[neighbour.index]
			}

			toCheckQueue = append(toCheckQueue, neighbour)
		}
	}

	return []*Line{} // We should not get here
}

func main() {
	//inp := "4 5\n0 1 1\n1 2 2\n2 3 3\n3 0 4\n0 2 8"
	datasetNumber := "06"
	//region////////// Load input //////////
	inpBytes, _ := ioutil.ReadFile("src/data/" + datasetNumber + ".in")
	inp := string(inpBytes)
	inpSplit := strings.Split(inp, "\n")

	firstSplit := strings.Split(inpSplit[0], " ")
	satelliteCount, _ := strconv.Atoi(firstSplit[0])
	linesCount, _ := strconv.Atoi(firstSplit[1])
	// endregion

	//region////////// Parse input //////////
	satellites := make([]*Satellite, satelliteCount)
	for i := 0; i < satelliteCount; i++ {
		satellites[i] = &Satellite{lines: []*Line{}, index: i}
	}

	lines := make([]*Line, linesCount)
	for i := 0; i < linesCount; i++ {
		lineSplit := strings.Split(inpSplit[i+1], " ")
		sat1, _ := strconv.Atoi(lineSplit[0])
		sat2, _ := strconv.Atoi(lineSplit[1])
		cost, _ := strconv.Atoi(lineSplit[2])
		lines[i] = &Line{sat1: satellites[sat1], sat2: satellites[sat2], cost: cost, index: i}
		satellites[sat1].lines = append(satellites[sat1].lines, lines[i])
		satellites[sat2].lines = append(satellites[sat2].lines, lines[i])
	}

	fmt.Println("loaded")
	sort.Slice(lines, func(i, j int) bool {
		return lines[i].cost < lines[j].cost
	})
	fmt.Println("sorted")
	fmt.Println()
	// endregion

	//region////////// Find min skell //////////
	now := time.Now()
	minCost, minSkell := getMinSkell(lines, satelliteCount)
	fmt.Println("Find min skell Took: ", time.Now().Sub(now))
	fmt.Println("min skell found, ", minCost)
	// endregion

	//region////////// Second min skell ////////// - http://web.mit.edu/6.263/www/quiz1-f05-sol.pdf
	now = time.Now()
	usedLines := minSkell
	notUsedLines := make([]*Line, 0, linesCount-len(minSkell))
	for lineSortedIndex := 0; lineSortedIndex < linesCount; lineSortedIndex++ {
		line := lines[lineSortedIndex]
		if !line.used {
			// Try to replace with this line
			notUsedLines = append(notUsedLines, line)
		}
	}

	////////////////////////////////////////////////

	fmt.Println()
	fmt.Println("Not used lines count: ", len(notUsedLines))
	var bestCycleLineAdd *Line
	var bestCycleLineRemove *Line
	secondBestCost := math.MaxInt64
	justForTime := time.Now()

	timer := time.Now()
	for i, line := range notUsedLines {
		if i%1000 == 0 {
			fmt.Printf("Visiting line % 6d/%d, time: %v\n", i+1, len(notUsedLines), time.Now().Sub(timer))
			timer = time.Now()
		}

		//cycle := getPath(line.sat1, line.sat2)
		//
		////Select the most costly line to be removed
		//toRemove := cycle[0]
		//for j := 1; j < len(cycle); j++ {
		//	if cycle[j].cost > toRemove.cost {
		//		toRemove = cycle[j]
		//	}
		//}

		toRemove := getCostliestLineDeepFirst(line.sat1, line.sat2, line, lines[0])

		costDiff := line.cost - toRemove.cost
		if costDiff < secondBestCost {
			secondBestCost = costDiff
			bestCycleLineAdd = line
			bestCycleLineRemove = toRemove
		}
	}
	fmt.Println("JustForTime: ", time.Now().Sub(justForTime))

	result := make([]*Line, 1, len(usedLines))
	result[0] = bestCycleLineAdd
	for _, line := range usedLines {
		if line != bestCycleLineRemove {
			result = append(result, line)
		}
	}

	fmt.Println("Second min skell Took: ", time.Now().Sub(now))
	fmt.Println()
	// endregion

	// region ////////// Write output //////////
	out := ""
	out += strconv.Itoa(minCost + secondBestCost)
	//out += strconv.Itoa(minCost)
	out += "\n"
	for i := 0; i < len(result); i++ {
		out += strconv.Itoa(result[i].index)
		//for i := 0; i < len(minSkell); i++ {
		//	out += strconv.Itoa(minSkell[i].index)
		out += " "
	}
	out += "-1"

	fmt.Println()
	fmt.Println(minCost + secondBestCost)
	//fmt.Println(out)

	_ = ioutil.WriteFile("src/data/"+datasetNumber+".out", []byte(out), 0644)
	// endregion
}
