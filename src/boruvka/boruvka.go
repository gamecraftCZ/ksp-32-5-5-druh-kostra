package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Line struct {
	sat1        int
	sat2        int
	cost        int
	index       int
	sortedIndex int
}

func root(a int, trees []int) int {
	//if trees[a] != -1 {
	//	trees[a] = trees[trees[a]] // Flattening of root - only one layer needed - not working xD
	//	return trees[a]
	//}
	for trees[a] != -1 {
		trees[a] = root(trees[a], trees) // Flattening of root
		a = trees[a]
	}
	return a
}

func find(a, b int, trees []int) bool {
	return root(a, trees) == root(b, trees)
}

func connect(a, b int, trees []int) {
	trees[root(b, trees)] = a
}

func getSkell(lines []Line, satellitesCount, dipThisIndex int) (int, []Line) {
	trees := make([]int, satellitesCount)
	for i := 0; i < satellitesCount; i++ {
		trees[i] = -1
	}
	minSkell := make([]Line, satellitesCount-1)

	connectionsIndex := 0
	cost := 0
	for i := 0; i < len(lines); i++ {
		if i == dipThisIndex {
			continue
		}
		if !find(lines[i].sat1, lines[i].sat2, trees) {
			connect(lines[i].sat1, lines[i].sat2, trees)
			lines[i].sortedIndex = i
			minSkell[connectionsIndex] = lines[i]
			cost += lines[i].cost
			connectionsIndex++
		}
	}

	return cost, minSkell
}

func main() {
	datasetNumber := "../06"
	////////// Load input //////////
	inpBytes, _ := ioutil.ReadFile("src/data/" + datasetNumber + ".in")
	inp := string(inpBytes)
	//inp := "4 5\n0 1 1\n1 2 2\n2 3 3\n3 0 4\n0 2 8"
	inpSplit := strings.Split(inp, "\n")

	firstSplit := strings.Split(inpSplit[0], " ")
	satelliteCount, _ := strconv.Atoi(firstSplit[0])
	linesCount, _ := strconv.Atoi(firstSplit[1])

	////////// Parse input //////////
	lines := make([]Line, linesCount)
	for i := 0; i < linesCount; i++ {
		lineSplit := strings.Split(inpSplit[i+1], " ")
		sat1, _ := strconv.Atoi(lineSplit[0])
		sat2, _ := strconv.Atoi(lineSplit[1])
		cost, _ := strconv.Atoi(lineSplit[2])
		lines[i] = Line{sat1: sat1, sat2: sat2, cost: cost, index: i}
	}

	fmt.Println("loaded")
	sort.Slice(lines, func(i, j int) bool {
		return lines[i].cost < lines[j].cost
	})
	for i, line := range lines {
		line.sortedIndex = i
	}
	fmt.Println("sorted")

	////////// Do the magic //////////
	now := time.Now()
	minCost, minSkell := getSkell(lines, satelliteCount, -1)
	fmt.Println("Find min skell Took: ", time.Now().Sub(now))
	fmt.Println("min skell found, ", minCost)

	secondSkellMinCost := math.MaxInt64
	var secondSkell []Line

	timer := time.Now()
	for i := 0; i < len(minSkell); i++ {
		//for i := 0; i < 100; i++ {
		if i%1000 == 0 {
			fmt.Printf("Visiting line % 6d/%d, time: %v\n", i+1, len(minSkell), time.Now().Sub(timer))
			timer = time.Now()
		}

		newCost, newSkell := getSkell(lines, satelliteCount, minSkell[i].sortedIndex)
		if minCost < newCost && newCost < secondSkellMinCost {
			secondSkell = newSkell
			secondSkellMinCost = newCost
		}
	}
	//fmt.Printf("Total time: %v\n", time.Now().Sub(timer))

	////////// Write output //////////
	out := ""
	out += strconv.Itoa(secondSkellMinCost)
	out += "\n"
	for i := 0; i < len(secondSkell); i++ {
		out += strconv.Itoa(secondSkell[i].index)
		out += " "
	}
	out += "-1"

	fmt.Println()
	fmt.Println(secondSkellMinCost)
	//fmt.Println(out)

	err := ioutil.WriteFile(datasetNumber+".out", []byte(out), 0644)
	if err != nil {
		fmt.Println("Save error: ", err)
	} else {
		fmt.Println("Save success")
	}
}
