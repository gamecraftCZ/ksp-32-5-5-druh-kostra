package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

type Line struct {
	sat1  *Satellite
	sat2  *Satellite
	cost  int
	index int
}

func (line Line) getNeighbour(currentSat *Satellite) *Satellite {
	if line.sat1.index == currentSat.index {
		return line.sat2
	} else {
		return line.sat1
	}
}

//type Cost struct {
//	sourceSat *Satellite
//	cost      int
//}

type Satellite struct {
	lines    []*Line
	bestCost int
	allCosts map[int]*int // Key of this map is index if incoming Line
	index    int
}

func (sat Satellite) getCostFromLine(line *Line) *int {
	cost, ok := sat.allCosts[line.index]
	if ok {
		return cost
	} else {
		newCost := math.MaxInt64
		sat.allCosts[line.index] = &newCost
		return &newCost
	}
}

func calculateCosts(satellites []*Satellite) {
	satellites[0].bestCost = 0
	toCheckQueue := []*Satellite{satellites[0]} // Add first satellite to queue

	for len(toCheckQueue) > 0 {
		sat := toCheckQueue[0]
		toCheckQueue = toCheckQueue[1:] // Remove satellite from queue - may not garbage collect, but ok in this case! https://stackoverflow.com/a/26863706/5647384

		for _, line := range sat.lines {
			neighbour := line.getNeighbour(sat)
			currentCost := neighbour.getCostFromLine(line)

			if sat.bestCost+line.cost < *currentCost { // currentCost is pointer to memory where the actual value is stored. We must use "*" to get the value
				*currentCost = sat.bestCost + line.cost
			}
			if sat.bestCost+line.cost < neighbour.bestCost {
				neighbour.bestCost = sat.bestCost + line.cost
				toCheckQueue = append(toCheckQueue, neighbour)
			}
		}
	}
}

func main() {
	////////// Load input //////////
	inp := "4 5\n0 1 1\n1 2 2\n2 3 3\n3 0 4\n0 2 8"
	//inpBytes, _ := ioutil.ReadFile("06.in")
	//inp := string(inpBytes)
	inpSplit := strings.Split(inp, "\n")

	firstSplit := strings.Split(inpSplit[0], " ")
	satelliteCount, _ := strconv.Atoi(firstSplit[0])
	linesCount, _ := strconv.Atoi(firstSplit[1])

	////////// Parse input //////////
	//lines := make([]Line, linesCount)
	now := time.Now()
	lines := make([]*Line, linesCount)
	satellites := make([]*Satellite, satelliteCount)
	for i := 0; i < satelliteCount; i++ {
		satellites[i] = &Satellite{index: i, lines: []*Line{}, bestCost: math.MaxInt64, allCosts: map[int]*int{}}
	}
	for i := 0; i < linesCount; i++ {
		lineSplit := strings.Split(inpSplit[i+1], " ")
		sat1Index, _ := strconv.Atoi(lineSplit[0])
		sat2Index, _ := strconv.Atoi(lineSplit[1])
		cost, _ := strconv.Atoi(lineSplit[2])
		lines[i] = &Line{sat1: satellites[sat1Index], sat2: satellites[sat2Index], cost: cost, index: i}
		satellites[sat2Index].lines = append(satellites[sat2Index].lines, lines[i])
		satellites[sat1Index].lines = append(satellites[sat1Index].lines, lines[i])
	}
	fmt.Println("TIME to parse: ", time.Now().Sub(now))

	////////// Do the magic //////////
	now = time.Now()
	calculateCosts(satellites)
	fmt.Println("TIME to find first skell: ", time.Now().Sub(now))

	now = time.Now()
	fmt.Println("TIME to find unwanted line: ", time.Now().Sub(now))

	now = time.Now()
	fmt.Println("TIME to find second skell: ", time.Now().Sub(now))

	////////// Write output //////////
	//out := ""
	//out += strconv.Itoa(secondSkellMinCost)
	//out += "\n"
	//for i := 0; i < len(secondSkell); i++ {
	//	out += strconv.Itoa(secondSkell[i].index)
	//	out += " "
	//}
	//out += "-1"
	//
	//fmt.Println(out)
	//
	//_ = ioutil.WriteFile("02.out", []byte(out), 0644)
}
